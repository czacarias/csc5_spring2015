/* 
 * File:   labweek7.cpp
 * Author: Charlie
 *
 * Created on April 22, 2015, 11:39 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/* Problem 1 - Power function */
double pwr(double base, int exp)
{
    double initial = 1.0;
    
    if(exp > 0)
    {
        for(int i = 0; i < exp; i++)
            initial *= base;
        
        return initial;
    }
    else if(exp < 0)
    {
        for(int i = 0; i > exp; i--)
            initial *= (1 / base);
        
        return initial;
    }
    else // (exp == 0)
    {
        return 1;
    }

}
int main(int argc, char** argv) 
{
    double base_num;
    int exp_num;
    
    cout << "Enter a base number: ";
    cin >> base_num;
    
    cout << "Enter an exponent: ";
    cin >> exp_num;

    cout << base_num << " to the " << exp_num << " power is: "
            << pwr(base_num, exp_num) << endl;
    
    return 0;
}

