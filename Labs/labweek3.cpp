/* 
 * File:   labweek3.cpp
 * Author: Charlie Zacarias
 * Assignment: Lab - Week 3
 * Created on March 3, 2015, 11:24 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // Problem 1.
    /*
    string var1 = "1";
    int var2 = 2;
    
    cout << var1 << "+" << var1 << "=" << var1 + var1 << endl;
    cout << var2 << "+" << var2 << "=" << var2 + var2 << endl;
    */
    // Problem 2.
   /*
    int singles, doubles, triples, home_runs;
    int at_bats = singles + doubles + triples + home_runs;
    
    cout << "Please enter the number of singles, doubles,"
            " triples, and home runs the batter has hit." << endl;
    cin >> singles >> doubles >> triples >> home_runs;
    
    cout << "Please enter the number of at-bats the batter has attempted."
            << endl;
    cin >> at_bats;
    
    float slugging_percentage = static_cast<float>(singles + (2 * doubles) + (3 * triples) 
          + (4 * home_runs)) / at_bats;
    
    cout << "The batter's slugging percentage is " << slugging_percentage << " .\n"; */
    
     // Problem 3.
    /*
    // Prompts user to enter two numbers.
    cout << "Please enter two numbers less than 1000." << endl;
    
    int x;
    int y;
    int z;
    
    cin >> x >> y;
    
    cout << "x: " << setw(5) << left << x << "y: " << setw(5) << y << endl;
    
    cout << setw(80) << setfill ('-') << "" << endl;
    
    z=x;
    
    x=y;
    y=z;
    y=y;
   
    cout << setfill (' ') << "x: " << setw(5) << x << "y: " << setw(5) << y << endl; */
    
    // Problem 4.
    /*
    int a=4;
    int b=0;
    
    if (a==4)
    {
        b=4;
    }
    else if(a==9)
    {
        b=5;
    }
    else
    {
        b=6;
    }
    
    cout << "The value of a is: " << a << "." << endl;
    cout << "The value of b is: " << b << "." << endl; */
    
 // Problem 5.
    
    // Defined variables.
    double payment;
    double purchase;
    double change;
    double stillNeeded;
   
    double dollar = 1.00;
    double quarter = 0.25;
    double dime = 0.10;
    double nickel = 0.05;
    double penny = 0.01;
    
    int change_dollar;
    int change_quarters;
    int change_dimes;
    int change_nickels;
    int change_pennies;
    
    double changeDiff_dollar;
    
    // Prompts user to enter amount owed.
    cout << "Please enter the amount you owe: " << endl;
    cout << "$";
    cin >> purchase;
    
    // Prompts user to enter payment.
    cout << "Please enter your payment: " << endl;
    cout << "$";
    cin >> payment;
    
    // Equations.
    change = payment - purchase + 0.005;

    // Dollars.
    change_dollar = change / dollar;

    // Quarters.
    change_quarters = ((change - change_dollar) / quarter);

    // Dimes.
    changeDiff_dollar = change - change_dollar;
    change_dimes = ((changeDiff_dollar - (quarter * change_quarters)) / dime);

    // Nickels.
    change_nickels = (changeDiff_dollar - (quarter * change_quarters)
            - (dime * change_dimes)) / nickel;

    // Pennies.
    change_pennies = ((changeDiff_dollar - (quarter * change_quarters)
            - (dime * change_dimes) - (nickel * change_nickels))
            / penny);
    
    // Amount still owed.
    stillNeeded = purchase - payment;
    
    if(payment < purchase)
    {
        cout << "You have not given enough money to make the purchase." << endl;
        cout << "You still owe: $" << stillNeeded << "." << endl;
    }
    else if(payment > purchase)
    {
        cout << "Thank you. Your change is $" << setprecision(2) << fixed <<
                change << "." << endl;
        cout << "That is: " << change_dollar << " dollar(s), " <<
                change_quarters << " quarter(s), " << change_dimes <<
                " dime(s), " << change_nickels << " nickel(s), " <<
                change_pennies << " pennies." << endl;
    }
    else
    {
        cout << "Thank you, your payment has been processed." << endl;
    }
    
    return 0;
}