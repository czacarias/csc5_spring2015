/* 
 * File:   labweek4.cpp
 * Author: Charlie Zacarias
 * Assignment: Lab Week 4
 * Created on March 10, 2015, 11:56 AM
 */

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
//    int numGrade;
//    
//    // Prompts user to enter a number.
//    cout << "Please enter your grade percentage." << endl;
//    cin >> numGrade;
//    
//    if(numGrade <= 100 && numGrade >= 90)
//    {
//        cout << "Your grade is an A! Good for you." << endl;
//    }
//    else if(numGrade < 90 && numGrade >= 80)
//    {
//        cout << "Your grade is a B. Not bad." << endl;
//    }
//    else if(numGrade < 80 && numGrade >= 70)
//    {
//        cout << "Your grade is a C." << endl;
//    }
//    else if(numGrade < 70 && numGrade >= 60)
//    {
//        cout << "Your grade is a D. Wow." << endl;
//    }
//    else if (numGrade < 60)
//    {
//        cout << "Your grade is an F. Good job!" << endl;
//    }
//    else
//    {
//        cout << "You did not enter an integer between 1 to 100." << endl;
//    }
 
////***********************
//// Problem 2
////***********************
//    // a.
//    int x=0;
//    
//    while(x < 10)
//    {
//        cout << x << endl;
//        x++;
//    }
    
    // b.

//**********************    
// Problem 3
//**********************
//    for(int i = 0; i < 10; i++)
//    {
//        cout << "Please enter a number: ";
//        cin >> i;
//        cout << "You entered: " << i << endl;
//        cout << endl;
//    }
    
//*************************   
// Problem 4
//*************************
    // Prompts user to enter a letter grade.
    cout << "Please enter your letter grade: ";
    string letter_grade;
    cin >> letter_grade;
    
    // Decision of grade percentage based off input.
    if(letter_grade == "A+")
    {
        cout << "Your grade percentage is 100%!" << endl;
    }
    else if(letter_grade == "A")
    {
        cout << "Your grade percentage is around 93-99.9." << endl;
    }
    else if(letter_grade == "A-")
    {
        cout << "Your grade percentage is around 90-92.9." << endl;
    }
    else if(letter_grade == "B+")
    {
        cout << "Your grade percentage is around 87-89.9." << endl;
    }
    else if(letter_grade == "B")
    {
        cout << "Your grade percentage is around 83-86.9." << endl;
    }
    else if(letter_grade == "B-")
    {
        cout << "Your grade percentage is around 80-82.9." << endl;
    }
    else
    {
        cout << "You did not enter a valid letter grade." << endl;
    }

//******************************
// Fun test
//******************************
//    int i = 0;
//    
//    while(i <= 100)
//    {
//        cout << i << '\t' << pow(i,2) << '\n';
//        ++i;
//    }
    
    return 0;
}

