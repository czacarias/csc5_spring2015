
#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;
/*
 * Problem 3 - Function
 */ 
void vectOutput(vector<int>& v);

void vectOutput(vector<int>& v)
{
    int size = v.size();
    
    cout << "This vector contains: ";
    
    for(int i = 0; i < size; i++)
    {
        cout << v[i] << " ";
    }
}

/*
 * Problem 4 - Function
 */
void vectFunct(vector<int>& v2);

void vectFunct(vector<int>& v2)
{
    int num;
    int count = 0;
    
    while(num != -1)
    {
        cin >> num;
        v2.push_back(num);
        ++count;
    }
    
    cout << "The vector contains: ";
    
    for(int i = 0; i < count - 1; i++)
        cout << v2[i] << " ";
}


int main(int argc, char** argv) 
{
/* Part 2 */

/*
 * Problem 1
 */
    vector<int> v1;
    v1.push_back(3);
    v1.push_back(6);
    v1.push_back(8);
    
    int size = v1.size();
    
    cout << "The vector contains: ";
    
    for(int i = 0; i < size; i++)
    {
        cout << v1[i] << " ";
    }
    
    cout << endl;
    
/*
 * Problem 2
 */
    int num;
    int count = 0;
    vector<int> numbers;
    
    cout << "Please enter some numbers (enter -1 to quit): ";
    
    while(num != -1)
    {
        cin >> num;
        numbers.push_back(num);
        ++count;
    }
    
    cout << "The vector contains: ";
    
    for(int i = 0; i < count - 1; i++) // (count - 1) to disregard the "-1"
    {
        cout << numbers[i] << " ";
    }
    
    cout << endl;

/*
 * Problem 3 - Driver
 */
    vector<int> v_1;
    
    v_1.push_back(12);
    v_1.push_back(1);
    v_1.push_back(45);
    
    vectOutput(v_1);
    
    cout << endl;
    
/*
 * Problem 4 - Driver
 */
    vector<int> v2;
    cout << "Please enter some numbers: ";
    
    vectFunct(v2);
    
    return 0;
}

