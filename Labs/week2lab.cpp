/* 
 * File:   main.cpp
 * Author: Charlie Zacarias
 * Assignment: Lab Week 2
 * Created on March 2, 2015, 11:47 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Problem 1**************
    // Creation of variable.
    string helloText;
    
    // Definition of variable.
    helloText = "Hello, my name is Hal!";
    
    // Output.
    cout << helloText << endl;
    
    // Input.
    // Hal asks for user's name.
    string userInput;
    cout << "What is your name?" << endl;
    cin >> userInput;
    
    // Response.
    cout << "Hello, " << userInput << ". I am glad to meet you.\n";
    
    // Problem 2***************
    int a=4;
    int b=12;
    int c=a;
    
    cout << "a: " << a << " " << "b: " << b << endl;
    
    a=b;
    b=c;
    b=c;
    
    cout << "a: " << a << " " << "b: " << b << endl;
    
    // Problem 3***************
    double number_in_meters, number_in_miles, number_in_feet, number_in_inches;
    
    cout << "Hello." << endl;
    cout << "This program will convert a measurement in meters\n"
            "to a measurement in miles, feet, and inches.\n";
    
    // Prompting user to enter.
    cout << "Please enter the number of meters you would like converted.\n";
    
    cin >> number_in_meters;
    
    number_in_miles = number_in_meters / 1609.344;
    number_in_feet = 3.281 * number_in_meters;
    number_in_inches = 39.370 * number_in_meters;
    
    //static_cast<double>(number_in_meters, number_in_miles, number_in_feet, number_in_inches);
    
    cout << number_in_meters << " meters is equivalent to " << number_in_miles << " miles.\n";
    cout << number_in_meters << " meters is equivalent to " << number_in_feet << " feet.\n";
    cout << number_in_meters << " meters is equivalent to " << number_in_inches << " inches.\n";
   
    // Problem 4***************
    
    // Problem 5***************
    double first_test_score, sec_test_score, third_test_score, total_score, average_score;
    
    cout << "Hello." << endl;
    cout << "This program will average your three test scores.\n";
    
    // Prompting user to enter.
    cout << "Please enter your three test scores with one space separating each one, then return.\n";
    cin >> first_test_score; 
    cin >> sec_test_score;
    cin >> third_test_score;
    
    total_score = first_test_score + sec_test_score + third_test_score;
    average_score = total_score / 3.0;
    
    // Output of average score.
    cout << "Your average score is " << average_score << ".\n";
    
    
    // Problem 6 (Challenge)**********
    int num=1-10000;
    
    // Prompts user to enter "x".
    cout << "Please enter a number between 1 and 10,000." << endl;
    
    cin >> num;
    
    cout << num << "Cool.\n";
    
    return 0;
}

