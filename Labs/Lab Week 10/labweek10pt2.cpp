/* 
 * File:   labweek10.cpp
 * Author: rcc
 *
 * Created on April 28, 2015, 11:35 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;

void arrayStore(int array[], int);

void arrayStore3(int array[], int);

bool checkValue(int array[], int, int);

int duplicateValue(int array[], int, int);

int duplicateLoc(int array[], int, int);

void removeDup(int array[], int&, int, int);

void outputArray(int array[], int);

int main(int argc, char** argv) 
{   
/* Problem 2 / 3 */
//    int a2[50];
//    arrayStore(a2, 50);
//    
//    cout << "Array a_2 contains: ";
//    
//    for(int i = 0; i < 50; i++)
//    {
//        cout << a2[i] << " ";
//    }
   
/* Problem 4 */
//   int value;
//   int a3[10];
//   
//   arrayStore(a3, 10);
//   
//   cout << "Please enter a value to check if it's inside the vector: ";
//   cin >> value;
//   
//   if(checkValue(a3, 10, value) == true)
//   {
//       cout << value << " is inside the array." << endl;
//   }
//   else
//   {
//       cout << value << " is not inside the array." << endl;
//   }
   
/* Problem 4 / 5 / 6 */
   int val;
   int size = 20;
   int a4[size];
   
   cout << "Please enter a value to check if it has duplicates"
           " inside the vector: ";
   cin >> val;
   
   arrayStore3(a4, size);
   cout << "The array contains: ";
   outputArray(a4, size);
   cout << endl;
   
   duplicateValue(a4, size, val);
   
   checkValue(a4, size, val);
   
   if(checkValue(a4, size, val) == true)
   {
       cout << val << " repeats " << duplicateValue(a4, 10, val) << " times."
               << endl;
   
       if(duplicateValue(a4, size, val) > 1)
       {
           cout << "The duplicate values will now be removed." << endl;

           int loc = duplicateLoc(a4, size, val);

           removeDup(a4, size, val, loc);
           
           outputArray(a4, size);
       }
   }
   else
   {
       cout << "The value does not exist in the vector." << endl;
   }
   
    
   return 0;
}

void arrayStore(int array[], int size)
{  
    for(int i = 0; i < size; i++)
    {
        array[i] = rand() % 51 + 50;
    }
}

void arrayStore3(int array[], int size)
{  
    for(int i = 0; i < size; i++)
    {
        array[i] = rand() % 20 + 1;
    }
}

bool checkValue(int array[], int size, int value)
{
    for(int i = 0; i < size; i++)
    {
        if(array[i] == value)
        {
            return true;
        }
    }
    return false;
}

int duplicateValue(int array[], int size, int value)
{
    // First check if value exists in vector.
    checkValue(array, size, value);
    
    if(checkValue(array, size, value) == true)
    {
        // Then check if value has duplicates.
        int countDups = 0;
        
        for(int i = 0; i < size; i++)
            if(array[i] == value)
                countDups++;
        
        return countDups++;
    }
}

int duplicateLoc(int array[], int size, int value)
{
    // First check if value exists in vector.
    checkValue(array, size, value);

    for(int i = 0; i < size; i++)
    {
        if(array[i] == value)
        {
            return i;
        }
    }
}

void removeDup(int array[], int& size, int value, int loc)
{   
    loc = duplicateLoc(array, size, value); // Make nested for loop.
    for(int i = loc; i < size - 1; i++)
    {
        array[i] = array[i+1];
    }
    size--;
}

void outputArray(int array[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
}