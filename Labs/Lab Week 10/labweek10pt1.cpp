/* 
 * File:   labweek10.cpp
 * Author: rcc
 *
 * Created on April 28, 2015, 11:35 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;

void vectStore(vector<int>&);

void vectStore3(vector<int>&);

bool checkValue(const vector<int>&, int);

int duplicateValue(const vector<int>&, int);

int duplicateLoc(const vector<int>&, int);

void swap(vector<int>&, int, int);

void removeDupVal(vector<int>&, int);

int main(int argc, char** argv) 
{
/* Problem 1 */
    string fileName;
    
    cout << "Please enter a file name: ";
    cin >> fileName;
    
    ifstream infile;
    infile.open(fileName.c_str());
    
    while(!infile)
    {
        cout << "ERROR! Please enter a valid file name: ";
        cin >> fileName;
        infile.open(fileName.c_str());
    }

    vector<int> v1;
    
    int value;
    
    while(infile >> value)
    {
        v1.push_back(value);
    }
    cout << endl;
    
    for(int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    
    infile.close();
    
    cout << "Vector v1 contains: ";
    
    for(int i = 0; i < v1.size(); i++)
        cout << v1[i] << " ";
    
/* Problem 2 */
    vector<int> v_1;
    vectStore(v_1);
    
    cout << "Vector v_1 contains: ";
    
    for(int i = 0; i < 50; i++)
    {
        cout << v_1[i] << " ";
    }
   
/* Problem 3 */
   int value;
   vector<int> v_3;
   
   cout << "Please enter a value to check if it's inside the vector: ";
   cin >> value;
   
   bool trueFalse = checkValue(v_3, value);
   
   if(trueFalse == true)
   {
       cout << value << " is inside the vector." << endl;
   }
   else
   {
       cout << value << " is not inside the vector." << endl;
   }
   
/* Problem 4 */
   int val;
   vector<int> v_4;
   
   cout << "Please enter a value to check if it has duplicates"
           " inside the vector: ";
   cin >> val;
   
   vectStore3(v_4);
   duplicateValue(v_4, val);
   
   checkValue(v_4, val);
   
   if(checkValue(v_4, val) == true)
   {
       cout << val << " repeats " << duplicateValue(v_4, val) << " times."
               << endl;
   }
   else
   {
       cout << "The value does not exist in the vector." << endl;
   }
    
/* Problem 5 */
//    int val;
//   vector<int> v_4;
//   
//   cout << "Please enter a value to check if it has duplicates"
//           " inside the vector: ";
//   cin >> val;
//   
//   vectStore3(v_4);
//   duplicateValue(v_4, val);
//   
//   checkValue(v_4, val);
//   
//   if(checkValue(v_4, val) == true)
//   {
//       cout << val << " repeats " << duplicateValue(v_4, val) << " times."
//               << endl;
//   }
//   else
//   {
//       cout << "The value does not exist in the vector." << endl;
//   }
   
   return 0;
}

/**
 * This function stores random value over 50 and between 100. The vector is of
 * size 50.
 * @param v -- Size 50 vector.
 * @param num -- Random number(s) to be stored in vector v.
 */
void vectStore(vector<int>& v)
{
    int num;
    
    for(int i = 0; i < 50; i++)
    {
        num = rand() % 51 + 50;
        v.push_back(num);
    }
}

void vectStore3(vector<int>& v)
{
    int num;
    
    for(int i = 0; i < 10; i++)
    {
        num = rand() % 20;
        v.push_back(num);
    }
}

bool checkValue(const vector<int>& v, int value)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == value)
        {
            return true;
        }
    }
    return false;
}

int duplicateValue(const vector<int>& v, int value)
{
    // First check if value exists in vector.
    checkValue(v, value);
    
    if(checkValue(v, value) == true)
    {
        // Then check if value has duplicates.
        int countDups = 0;
        
        for(int i = 0; i < v.size(); i++)
            if(v[i] == value)
                countDups++;
        
        return countDups++;
    }
}

void swap(vector<int>& v, int, int)
{
    
    
}

int duplicateLoc(const vector<int>& v, int value)
{
    // First check if value exists in vector.
    checkValue(v, value);
    
    // Then check how many times the value is duplicated.
    duplicateValue(v, value);
    
    if(checkValue(v, value) == true && duplicateValue(v, value) > 1)
    {
        
    }
}

void removeDupVal(vector<int>& v, int loc)
{

}