/* 
 * File:   labweek6.cpp
 * Author: Charlie Zacarias
 * Assignment: Lab Week 6
 * Created on April 2, 2015, 11:20 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

// Functions.

// Problem 1 Function.
int largest()
{
    
}
int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
//    cout << "******** Problem 1 ********" << endl;
//    cout << endl;
//    
//    srand(time(0));
//    int num = 10;
//    int val[num];
//    int max;
//    string answer;
//    
//    cout << "This program will generate 10 values and compare "
//            "them to see which one is the largest." << endl;
//    cout << "Would you like to run this program? (Enter \"Yes\" to run, or "
//            "\"No\" to cancel): ";
//    cin >> answer;
//    
//    while(answer == "Yes" || answer == "yes")
//    {
//        for(int i = 0; i < num; i++) 
//        {   
//            val[i] = rand() % 100 + 1; // Random value from 1 to 100.
//        }
//        
//        cout << endl;
//        cout << "The numbers generated were: ";
//
//        for(int i = 0; i < num; i++)
//        {
//            cout << val[i] << " ";
//        }
//        cout << endl;
//        
//        for(int i = 0; i + 1 < num; i++)
//        {
//            if (val[i] > val[i+1])
//            {
//                max = val[i];
//            }
//            else if(val[i+1] > val[i])
//            {
//                max = val[i+1];
//            }
//            else
//            {
//                max = val[i];
//            }
//        }
//        
//        cout << "The largest value is " << max << "." << endl;
//        answer = "no";
//    }
    
//**********************
// Problem 2 / Problem 3
//**********************
//    cout << "******** Problem 2 / Problem 3 ********" << endl;
//    cout << endl;
//    
//    cout << "This program will allow the user to read from a file (.txt) \n"
//            "and output the number of characters within that file." << endl;
//    cout << endl;
//    
//    // Outputs text to file.
//    ofstream outfile;
//    outfile.open("problem2.txt");
//    cout << "Please enter some text: " << endl;
//    
//    string user_text;
//    cin >> user_text;
//    
//    
//    outfile << user_text << endl;
//    
//    // Reads input from file.
//    ifstream infile;
//    infile.open("problem2.txt");
//    
//    infile >> user_text;
//    getline(cin, user_text); // From sec. 11.5 of my textbook.
//                             // Not sure if anticipated way of doing this
//                             // problem. Basically getline() reads in the
//                             // entire line, disregarding the white-spaces,
//                             // whereas a string itself stops are a white-space.
//                             // Also not sure if you want punctuation to count
//                             // as characters.
//    
//    int numChar = user_text.size() + 1; // "+ 1" to count last punctuation mark.
//                                        // Apparently the last punct. mark isn't
//                                        // counted.
//    
//    cout << "The number of characters is: " << numChar << endl;
//                                  
//    
//    infile.close();
//    
//    // Problem 3.
//    ofstream outfile_numChar;
//    
//    outfile.open("Problem2_numChar.txt");
//    
//    string enter_numChar; // User enters numChar into any document.
//    cout << "Enter the number of characters to save into a .txt document."
//            << endl;
//    cin >> enter_numChar;
//    outfile << enter_numChar;
//    outfile_numChar.close();
    
//**********************
// Problem 4
//**********************
    cout << "******** Problem 4 ********" << endl;
    cout << endl;
    
//    double n;
//    
//    cout << "Please enter a number: ";
//    cin >> n;
//
//    double x = 0;
//    double s;
//    
//    do 
//    {
//        s = 1.0 / (1 + n * n);
//        n++;
//        x = x + s;
//    } 
//    while (s > 0.01);
//    
//    cout << x;
    
    double n;
    
    cout << "Please enter a number: ";
    cin >> n;
    
    double x = 0;
    double s;
    
    while(s > 0.01)
    {   
        s = 1.0 / (1 + n * n);        
        n++;
        x = x + s; 
    }
    return 0;
}
