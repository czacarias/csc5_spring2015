/* 
 * File:   labweek4pt2.cpp
 * Author: Charlie
 * Assignment: Lab Week 4 -- Pt. 2
 * Created on March 14, 2015, 6:57 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
    string letter_grade;
    
    // Prompts user to enter a letter grade.
    cout << "This program will convert your letter grade into \n"
            "its respective grade-point." << endl;
    cout << "Please enter your letter grade (from A+ to B-): ";
    cin >> letter_grade;
    
    // When user inputs A+, A, or A-.
    if(letter_grade.substr(0,1) == "A")
    {
        if(letter_grade.substr(0,2) == "A+" || letter_grade.substr(0,2) == "A")
        {
            cout << "Your grade-point is a 4.0." << endl;
            cout << endl;
        }
        if(letter_grade.substr(0,2) == "A-" && letter_grade.substr(0,2) != "A+")
        {
            cout << "Your grade-point is a 3.7." << endl;
            cout << endl;
        }
    }
    // When user inputs B+, B, B-.
    // A if-statements are ignored if user entered a B-grade.
    else if(letter_grade.substr(0,1) == "B")
    {
        if(letter_grade.substr(0,2) == "B+" && letter_grade.substr(0,2) != "B")
        {
            cout << "Your grade-point is a 3.3." << endl;
            cout << endl;
        }
        if(letter_grade.substr(0,2) == "B" && letter_grade.substr(0,2) != "B+"
                && letter_grade.substr(0,2) != "B-")
        {
            cout << "Your grade-point is a 3.0." << endl;
            cout << endl;
        }
        if(letter_grade.substr(0,2) == "B-" && letter_grade.substr(0,2) != "B+")
        {
            cout << "Your grade-point is a 2.7." << endl;
            cout << endl;
        }
    }
    else // True for all A if-statements and B if-statements.
    {
        cout << "You did not enter a valid letter-grade.";
        cout << endl;
    }
    
//**********************
// Problem 2
//**********************
//    int i = 0, n;
//    double guess, prev_guess, r, ans_n;
//    
//    // Prompts user to input an integer n.
//    cout << "Please enter an integer." << endl;
//    cin >> n;
//    
//    // Prompts user to input a guess.
//    cout << "This program will compute the square root of your integer." <<
//            endl;
//    cout << "Please enter a guess of its square root." << endl;
//    cin >> guess;
//    
//    // Equations.
//    n > 0;
//    r = n / guess;
//    guess = (guess + r) / 2;
//    
//    // Algorithm code
//    while(guess <= prev_guess * 0.99 /*&& guess >= prev_guess * 1.01*/) 
//        // 1% of prev_guess.
//    {
//        r = n / guess;
//        prev_guess = guess;
//        guess = (guess + r) / 2;
//        i++;
//    }
//    
//    // Outputs answer.
//    cout << "Your answer is: " << setprecision (5) << fixed << guess << ".";
//    cout << endl;
    
//**********************
// Problem 3
//**********************
//    int year;
//    
//    
//    // Prompts user to enter a year.
//    cout << "Please enter a year (yyyy): ";
//    cin >> year;
//    
//    if(year % 400 == 0)
//    {
//        cout << "The year " << year << " is a leap year!" << endl;
//    }
//    else if(year % 100 == 0)
//    {
//        cout << "The year " << year << " is not a leap year." << endl;
//    }
//    else if(year % 4 == 0)
//    {
//        cout << "The year " << year << " is a leap year!" << endl;
//    }
//    else
//    {
//        cout << "The year " << year << " is not a leap year." << endl;
//    }
 
    
//**********************
// Problem 4
//**********************
//    int leap_year;
//    
//    // Prompts user to enter a year.
//    cout << "Please enter a year (yyyy): ";
//    cin >> leap_year;
//
//    if (leap_year % 4 == 0 && (leap_year % 100 != 0 || leap_year % 400 == 0))
//    {
//        cout << "The year " << leap_year << " is a leap year!" << endl;
//    }
//    else
//    {
//        cout << "The year " << leap_year << " is not a leap year." << endl;
//    }

//**********************
// Problem 5
//**********************
//    srand(time(0)); // Random number; generates new one every loop.
//    int secret = rand() % 100 + 1; // Randomly generates a number 1 to 100.
//    int guess;
//    int guesses = 0;
//    string answer;
//    
//    // Prompts user to make a guess.
//    cout << "Welcome to the Guessing Game." << endl << "This program will "
//            "randomly select a number between 1 and 100." << endl <<
//            "As the user, you are to guess what the secret number is." << endl;
//    cout << "Would you like to play a game?" << endl
//            << "Enter 'Y' for 'Yes' or 'N' for 'No': ";
//    cin >> answer;
//    
//    while(answer == "Yes" || answer == "yes")
//    {
//        cout << "Please enter your guess: ";
//        cin >> guess;
//        guesses++;
//        
//        if(guess == 0 || guess > 100)
//        {
//            cout << "That is not a valid guess. Guess again." << endl;
//            cout << endl;
//        }
//        if(guess > secret)
//        {
//            cout << "Your guess is too high! Guess again." << endl;
//        }
//        if(guess < secret)
//        {
//            cout << "Your guess is too low! Guess again." << endl;
//        }
//        if(guesses < 10)
//        {
//            cout << "You have " << (10 - guesses) << " guesses left." << endl;
//            cout << endl;
//        }
//        if(guess == secret)
//        {
//            cout << "Congratulations!!!" << endl <<
//                    "You have guessed the correct number!" << endl;
//            cout << "Number of guesses: " << guesses << endl;
//            answer = "no";
//        }
//        if(guesses == 10)
//        {
//            cout << "You have ran out of guesses!" << endl
//                    << "Please play again." << endl;
//            guesses = 0;
//            answer = "no";
//        }
//        if(answer == "no")
//        {
//            cout << "Would you like to play the game again?" << endl
//                    << "Please enter 'Y' for Yes' or 'N' for 'No': ";
//            cin >> answer;
//            cout << endl;
//        }
//        if(answer == "No" || answer == "no")
//        {
//            cout << "Thanks for playing!" << endl;
//        }
//    } 
    
    return 0;
}