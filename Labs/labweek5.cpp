/* 
 * File:   labweek5.cpp
 * Author: Charlie Zacarias
 * Assignment: Lab Week 5
 * Created on March 24, 2015, 11:22 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
    cout << "******** Problem 1 ********" << endl;
    cout << endl;
    
    double balance, sub_balance, remaining_balance, new_balance1,
            new_balance2, tot_balance;
    double interest1_5; // 1.5% interest.
    double interest1_0; // 1.0% interest.
    double min_payment;
    
    string answer;
    
    // Program's purpose.
    cout << "Hello. This program will compute the interest due, the total"
            " amount due, and the minimum payment for your credit account."
            << endl;
    
    cout << "Would you like to utilize this program (Please type 'Yes' or 'No')"
            "?: ";
    cin >> answer;
    
    cout << endl;
    
    while(answer == "Yes" || answer == "yes") 
    {
        // Prompts user to input account balance.
        cout << "Please enter your account's balance: $";
        cin >> balance;
        
        // Equations to separate balance from $1000 and remainder.
        sub_balance = balance - 1000.00;
        remaining_balance = balance - sub_balance;
        
        if(balance)
        {
            // Interest calculated for the first $1000 limit.
            interest1_5 = remaining_balance * 0.015;
            
            // 1.5% interest added to the first $1000 limit.
            new_balance1 = remaining_balance + interest1_5;
            
            // Interest calculated for the remaining money after $1000 limit.
            interest1_0 = sub_balance * 0.01;
            
            // 1.0% interest added to the remaining money.
            new_balance2 = sub_balance + interest1_0;
            
            // Total balance.
            tot_balance = new_balance2 + new_balance1;
            
            // Minimum payment.
            min_payment = tot_balance * 0.10;
        }
        if(min_payment > 10.00)
        {
            cout << "Your total amount due is: $" << fixed <<
                    setprecision(2) << tot_balance << endl;
            cout << "A $" << min_payment << " minimum payment is required."
                    << endl;
            answer = "No";
        }
        if(min_payment <= 10.00)
        {
            cout << "Your total amount due is: $" << fixed <<
                    setprecision(2) << tot_balance << endl;
            cout << "A $10.00 minimum payment is required."
                    << endl;
            answer = "No";
        }
        if(answer == "No")
        {
            cout << "Thank you for using this program." << endl;
            cout << "Would you like to use this program again?" << endl <<
                    "Please type 'Yes' to use again, or 'No' to end." << endl;
            cin >> answer;
            cout << endl;      
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you for using this program." << endl;
        }
    }
    
//**********************
// Problem 2
//**********************
    cout << "******** Problem 2 ********" << endl;
    cout << endl;
    
    string answer;
    int toothpicks = 23;
    int user_draw;
    int comp_draw;
    
    
    // Prompts user to start game.
    cout << "Would you like to play the game of '23'?" << endl;
    cout << "'Yes' to start, 'No' to cancel: ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter how many toothpicks you would like to withdraw "
                "(from 1 to 3): ";
        cin >> user_draw;
        
        cout << "You withdrew " << user_draw << " toothpicks." << endl;
        toothpicks -= user_draw;
        cout << "There are " << toothpicks << " toothpicks left." << endl;
        cout << endl;
        
        if(toothpicks > 4 && user_draw <= 3)
        {
            comp_draw = 4 - user_draw;
            
            cout << "The computer withdrew " << comp_draw << " toothpicks."
                    << endl;
            toothpicks -= comp_draw;
            cout << "There are " << toothpicks << " toothpicks left." << endl;
            
            answer = "Yes";
        }   
        else if(toothpicks <= 4 && toothpicks >= 2)
        {
            comp_draw =  toothpicks - 1;
            
            cout << "The computer withdrew " << comp_draw << " toothpicks."
                    << endl;
            toothpicks -= comp_draw; 
            cout << "There are " << toothpicks << " toothpicks left." << endl;

            answer = "Yes";
        }
        else if(toothpicks == 1)
        {
            comp_draw = 1;
            
            cout << "The computer drew " << comp_draw << " toothpicks." << endl;
            toothpicks -= comp_draw;
            cout << "There are " << toothpicks << " toothpicks left." << endl;
            cout << "Congratulations! You have won!" << endl;
            cout << endl;
            
            answer = "no";
        }
     
        else
        {
            cout << "Sorry, you have lost the game." << endl;
            answer = "no";
        }
        
        if(answer == "no")
        {
            cout << "Would you like to play again ('Yes' or 'No')?: ";
            cin >> answer;
            toothpicks = 23;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you for playing!" << endl;
        }  
       
    }
    
//**********************
// Problem 3
//**********************
    cout << "******** Problem 3 ********" << endl;
    cout << endl;

    string word;
    string answer;
    bool is_palindrome;
    bool not_palindrome;
    
    // Prompts user to enter a word.
    cout << "This program will check if the word you enter is a palindrome "
            "or not." << endl;
    cout << "Would you like to run this program? (\"Yes\" to run, or \"No\" to"
            " cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter a word: ";
        cin >> word;

        for(int i = 0; i < word.size(); i++)
        {
            if(word[i] != word[word.size() - 1 - i])
            {
                not_palindrome = false;
            }
            else
            {
                not_palindrome = true;
            }
        }
        
        if(not_palindrome == false)
        {
            cout << "\"" << word << "\"" << " is not a palindrome." << endl;
        }
        else
        {
            cout << "\"" << word << "\"" << " is a palindrome!~" << endl;
        }
        
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to enter another word? (\"Yes\" to run, "
                    "or \"No\" to cancel): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you, the program will terminate." << endl;
        }
    }
    
//**********************
// Problem 4
//**********************
    cout << "******** Problem 4 ********" << endl;
    cout << endl;
    
    int num_stars;
    string answer;
    
    // Prompts user to start program.
    cout << "Hello, this program will print a pretty picture." << endl;
    cout << "Would you like to run this program? (\"Yes\" to start, or \"No\" "
            "to cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter the number of stars you would like each "
                "row to have." << endl;
        cout << "Number of stars (*): ";
        cin >> num_stars;
        
        if(num_stars > 0)
        {
            for(int l = 0; l < num_stars / 2; l++) // # of "_"s.
            { 
                for(int i = 0; i < (2 * l) + 1; i++) // Rows.
                {
                    cout << "* ";
                }
                
                cout << "_ "; // Outputs once every row. Refer to l for-loop.
                              // Outputs new indented "_ " for each clmn/row.
                for(int j = 0; j < (num_stars - 1 - (2 * l)); j++) // Columns.
                {
                    cout << "* ";
                }
                
                cout << endl;
            }
        }
        else
        {
            cout << "You did not enter a valid number." << endl;
        }
        
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to run the program again? (Enter \"Yes\" to"
                    " run again, or \"No\" to cancel.): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you." << endl;
        }
    }
    
    return 0;
}
