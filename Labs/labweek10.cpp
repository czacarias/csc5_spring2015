/* 
 * File:   labweek10.cpp
 * Author: rcc
 *
 * Created on April 28, 2015, 11:35 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;

void vectStore(vector<int>&);

void vectStore3(vector<int>&);

bool findVal(const vector<int>&, int);

int main(int argc, char** argv) 
{
/* Problem 1 */
//    string fileName;
//    
//    cout << "Please enter a file name: ";
//    cin >> fileName;
//    
//    ifstream infile;
//    infile.open(fileName.c_str());
//    
//    while(!infile)
//    {
//        cout << "ERROR! Please enter a valid file name: ";
//        cin >> fileName;
//        infile.open(fileName.c_str());
//    }
//    cout << infile << endl;
//    
//    
//    vector<int> v1;
//    
//    for(int i = 0; i < ; i++)
//        cout << << " ";
//    
//    infile.close();
//    
//    cout << "Vector v1 contains: ";
//    
//    for(int i = 0; i < v1.size(); i++)
//        cout << v1[i] << " ";
    
/* Problem 2 */
//    vector<int> v_1;
//    vectStore(v_1);
//    
//    cout << "Vector v_1 contains: ";
//    
//    for(int i = 0; i < 50; i++)
//    {
//        cout << v_1[i] << " ";
//    }
    
/* Problem 3 */
    vector<int> v_2;
    int value;
    
    vectStore3(v_2);
    
    
    cout << "Please enter a value you would like to check if it is inside "
            "the vector: ";
    cin >> value;
    
    if(findVal(v_2, value) == true)
    {
        cout << "The value is contained within the vector." << endl;
    }
    else
    {
        cout << "The value is not contained within the vector." << endl;
    }
    
    return 0;
}

/**
 * This function stores random value over 50 and between 100. The vector is of
 * size 50.
 * @param v -- Size 50 vector.
 * @param num -- Random number(s) to be stored in vector v.
 */
void vectStore(vector<int>& v)
{
    srand(time(0));
    int num;
    
    for(int i = 0; i < 50; i++)
    {
        num = rand() % 51 + 50;
        v.push_back(num);
    }
}

void vectStore3(vector<int>& v)
{
    srand(time(0));
    int num;
    
    for(int i = 0; i < 25; i++)
    {
        num = rand() % 100;
        v.push_back(num);
    }
}
/**
 * This function is for problem 3 and checks a vector to see if a given value
 * is contained within the vector. If the value is in the vector the bool
 * function returns true, otherwise it will return false.
 * @param v - Any vector.
 * @param num - The value that the user to checking is within the vector.
 * @return - Returns true if the value is within the vector.
 *           Returns false if the value is not within the vector.
 */
bool findVal(const vector<int>& v, int num)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == num)
        {
            return true;
        }
    }
    return false;
}

