
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

// Function | Problem 5.
double mpg(double liters, double traveled_mi)
{
    const double LpG = 0.264172; // Liters per Gallon.
    double gallons = liters * LpG;
    double mpg = gallons / traveled_mi;

    return mpg;
}

int main(int argc, char** argv)
{
//**********************
// Problem 5
//**********************
    cout << "******** Problem 5 ********" << endl;
    cout << endl;
    
    string answer;
    
    // Prompts user about program.
    cout << "Hello, this program will calculate the number of miles per "
            "gallon (MPG) your car delivered relative to the distance "
            "traveled." << endl;
    cout << "Would you like to run this program? (Enter \"Yes\" to start, or "
            "\"No\" to cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
     
        cout << "Please enter the number of liters consumed: ";
        
        ofstream outfile;
        outfile.open("data.dat");
        double liters;
        cin >> liters;
        outfile << liters;
        
        cout << "Please enter the number of miles traveled: ";
        
        double traveled_mi;
        cin >> traveled_mi;
        outfile << traveled_mi;

        outfile.close();
        
        ifstream infile;
        infile.open("data.dat");
        
        infile >> liters >> traveled_mi;
        
        cout << endl;
                
        cout << "Your vehicle delivered " << fixed << setprecision(2)
                << mpg(liters, traveled_mi) << " miles per gallon." << endl;
        
        infile.close();
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to run the program again? (Enter \"Yes\" "
                    "to run again, or \"No\" to cancel): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you for using the program." << endl;
        }
    }
    

    return 0;
}
