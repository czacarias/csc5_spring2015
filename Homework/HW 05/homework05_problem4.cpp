
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;
// Functions:
// Problem 3 / 4
void problem1()
{
    string paper = "P";
    string scissors = "S";
    string rock = "R";
    
    int player1_wins = 0;
    int player2_wins = 0;
    
    string player1;
    string player2;
    
    string answer;
    
    // Prompts user to play game.
    cout << "Hello, this is a game of rock, paper, and scissors." << endl;
    cout << "This is a two-player game whom verses each other." << endl;
    cout << "Would you like to play? (Enter 'Yes' to start, or 'No' to cancel)"
            ": ";
    cin >> answer;
    
    cout << "Remember: paper covers rock, rock break scissors, "
                "scissors cut paper, or if the same choice, nobody wins."
                << endl;
    cout << endl;
        
    while(answer == "Yes" || answer == "yes")
    {

        cout << "Player 1, please select your choice: "
                << "Enter 'P' for paper, 'S' for scissors, or 'R' for rock."
                << endl;
        cout << "Player 1: ";
        cin >> player1;
        
        cout << "Player 2, please select your choice: "
                << "Enter 'P' for paper, 'S' for scissors, or 'R' for rock."
                << endl;
        cout << "Player 2: ";
        cin >> player2;

        if(player1 == "P" || player1 == "p")
        {
            if(player2 == "S" || player2 == "s")
            {
                cout << "Scissors beats paper! Player 2 wins." << endl;
                player2_wins++;
            }
            else if(player2 == "R" || player2 == "r")
            {
                cout << "Paper beats rock! Player 1 wins." << endl;
                player1_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(player1 == "S" || player1 == "s")
        {
            if(player2 == "P" || player2 == "p")
            {
                cout << "Scissors beats paper! Player 1 wins." << endl;
                player1_wins++;
            }
            else if(player2 == "R" || player2 == "r")
            {
                cout << "Rock beats scissors! Player 2 wins." << endl;
                player2_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(player1 == "R" || player1 == "r")
        {
            if(player2 == "S" || player2 == "s")
            {
                cout << "Rock beats scissors! Player 1 wins." << endl;
                player1_wins++;
            }
            else if(player2 == "P" || player2 == "p")
            {
                cout << "Paper beats rock! Player 2 wins." << endl;
                player2_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        cout << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to play again? ('Yes' to keep playing, or"
                    " 'No' to stop): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Player 1 won " << player1_wins << " match(es)." << endl;
            cout << "Player 2 won " << player2_wins << " match(es)." << endl;
            cout << "Thank you for playing!" << endl;
        }
    }
}

void problem2()
{
    srand(time(0));
    int paper = 1;
    int scissors = 2;
    int rock = 3;
    
    int comp1_wins = 0;
    int comp2_wins = 0;
    
    
    string answer;
    
    // Prompts user to play game.
    cout << "Hello, this is a game of rock, paper, and scissors." << endl;
    cout << "This is a two-player *computer* game whom verses each other." 
            << endl;
    cout << "Would you like to play? (Enter 'Yes' to start, or 'No' to cancel)"
            ": ";
    cin >> answer;
    
    cout << "Remember: paper covers rock, rock break scissors, "
                "scissors cut paper, or if the same choice, nobody wins."
                << endl;
    cout << endl;
        
    while(answer == "Yes" || answer == "yes")
    {

        cout << "Computer 1 is choosing..." << endl;
        int comp1 = rand() % 3 + 1;
        
        cout << "Computer 2 is choosing..." << endl;
        int comp2 = rand() % 3 + 1;
        
        if(comp1 == 1)
        {
            if(comp2 == 2)
            {
                cout << "Computer 1 chose paper!" << endl;
                cout << "Computer 2 chose scissors!" << endl;
                
                cout << "Scissors beats paper! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else if(comp2 == 3)
            {
                cout << "Computer 1 chose paper!" << endl;
                cout << "Computer 2 chose rock!" << endl;
                
                cout << "Paper beats rock! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose paper!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(comp1 == 2)
        {
            if(comp2 == 1)
            {
                cout << "Computer 1 chose scissors!" << endl;
                cout << "Computer 2 chose paper!" << endl;
                
                cout << "Scissors beats paper! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else if(comp2 == 3)
            {
                cout << "Computer 1 chose scissors!" << endl;
                cout << "Computer 2 chose rock!" << endl;
                
                cout << "Rock beats scissors! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose scissors!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(comp1 == 3)
        {
            if(comp2 == 2)
            {
                cout << "Computer 1 chose rock!" << endl;
                cout << "Computer 2 chose scissors!" << endl;
                
                cout << "Rock beats scissors! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else if(comp2 == 1)
            {
                cout << "Computer 1 chose rock!" << endl;
                cout << "Computer 2 chose paper!" << endl;
                cout << "Paper beats rock! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose rock!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        cout << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to play again? ('Yes' to keep playing, or"
                    " 'No' to stop): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Computer 1 won " << comp1_wins << " match(es)." << endl;
            cout << "Computer 2 won " << comp2_wins << " match(es)." << endl;
            cout << "Thank you for playing!" << endl;
        }
    }
}

int main(int argc, char** argv) 
{
//**********************
// Problem 3 / Problem 4
//**********************
    cout << "******** Problem 3 / 4 ********" << endl;
    cout << endl;
    
    int answer;
    
    // Prompts user to pick a game.
    cout << "Hello, welcome to Rock, Paper, Scissors!" << endl;
    cout << "Please enter \"1\" to play a two-player game, or enter \"2\" for "
            "a two-computer game: ";
    cin >> answer;
    
    switch(answer)
    {
        case 1:
            problem1();
            break;
        case 2:
            problem2();
            break;
        default:
            cout << "You did not enter a valid choice. Run again." << endl;
    }
    
    return 0;
}

