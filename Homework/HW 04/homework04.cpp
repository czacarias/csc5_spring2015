/*
* Name: Charlie Zacarias
* Student ID: 2493977
* Date: March 25, 2015
* HW: 04
* Problem: 1-9
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
    cout << "******** Problem 1 ********" << endl;
    cout << endl;
    
    int size = 10;
    int num[size]; // "[size]" values numbers to a max of "size" (which is 10).
    int sum_pos = 0; // Sum > 0.
    int sum_neg = 0; // Sum < 0.
    int sum_tot; // Sum of all #s.
    double avg_pos; // Average of # > 0.
    double avg_neg; // Average of # < 0.
    double avg_tot; // Total average of all #s.
    int count_pos = 0; // Counting #s > 0 starts at 0.
    int count_neg = 0; // Counting #s < 0 starts at 0.
    int start = 1; // Starts while-loop automatically.
    
    // Prompts user to enter 10 whole numbers.
    cout << "Please enter ten whole numbers:" << endl;
    
    // User inputs 10 whole numbers; negative or positive.
    while(start)
    {
        for(int i = 0; i < size; i++)
        {
            cout << i + 1 << ") ";
            cin >> num[i];

            if(num[i] >= 0)
            {
                ++count_pos;
                sum_pos += num[i];
                avg_pos = static_cast<double>(sum_pos) 
                        / (static_cast<double>(count_pos) + 0.000001);
            }
            else
            {
                ++count_neg;
                sum_neg += num[i];
                avg_neg = static_cast<double>(sum_neg) 
                        / (static_cast<double>(count_neg) + 0.000001);
            }

            start = 0; // False. Ends while loop after "size" (10) inputs.
        }
    }
    // Outputs numbers entered. "[i]" outputs the i'th number entered above.
    cout << "The numbers you entered were: ";
    for(int i = 0; i < size; i++ )
    {
         cout << num[i] << ", ";
    }
    
    sum_tot = sum_pos + sum_neg;
    avg_tot = (static_cast<double>(sum_pos) + static_cast<double>(sum_neg)) 
            / size;
    
    cout << endl << endl;
    cout << "The sum of the positive numbers is: " << sum_pos << endl;
    cout << "The average of the positive numbers is: " << avg_pos << endl;
    cout << endl;
    cout << "The sum of the negative numbers is: " << sum_neg << endl;
    cout << "The average of the negative numbers is: " << avg_neg << endl;
    cout << endl;
    cout << "The total sum of the numbers is: " << sum_tot << endl;
    cout << "The total average of the numbers is: " << avg_tot << endl;
    
//**********************
// Problem 2
//**********************
    cout << "******** Problem 2 ********" << endl;
    cout << endl;
    
    srand(time(0));
    int size = 10;
    int num[size]; // "[size]" values numbers to a max of "size" (which is 10).
    int sum_pos = 0; // Sum > 0.
    int sum_neg = 0; // Sum < 0.
    int sum_tot; // Sum of all #s.
    double avg_pos; // Average of # > 0.
    double avg_neg; // Average of # < 0.
    double avg_tot; // Total average of all #s.
    int count_pos = 0; // Counting #s > 0 starts at 0.
    int count_neg = 0; // Counting #s < 0 starts at 0.
    int start = 1; // Starts while-loop automatically.
    
    // Prompts user to enter 10 whole numbers.
    cout << "The computer will generate ten random numbers from -100 to 100."
            << endl;
    
    // User inputs 10 whole numbers; negative or positive.
    while(start)
    {
        for(int i = 0; i < size; i++)
        {
            num[i] = rand() % 200 - 100; 
            // Comp picks random # from -100 to 100.

            if(num[i] >= 0)
            {
                ++count_pos;
                sum_pos += num[i];
                avg_pos = static_cast<double>(sum_pos) 
                        / (static_cast<double>(count_pos) + 0.000001);
            }
            else
            {
                ++count_neg;
                sum_neg += num[i];
                avg_neg = static_cast<double>(sum_neg) 
                        / (static_cast<double>(count_neg) + 0.000001);
            }

            start = 0; // False. Ends while loop after "size" (10) inputs.
        }
    }
    // Outputs numbers entered. "[i]" outputs the i'th number entered above.
    cout << "The random numbers are: ";
    for(int i = 0; i < size; i++ )
    {
         cout << num[i] << ", ";
    }
    
    sum_tot = sum_pos + sum_neg;
    avg_tot = (static_cast<double>(sum_pos) + static_cast<double>(sum_neg)) 
            / size;
    
    cout << endl << endl;
    cout << "The sum of the positive numbers is: " << sum_pos << endl;
    cout << "The average of the positive numbers is: " << avg_pos << endl;
    cout << endl;
    cout << "The sum of the negative numbers is: " << sum_neg << endl;
    cout << "The average of the negative numbers is: " << avg_neg << endl;
    cout << endl;
    cout << "The total sum of the numbers is: " << sum_tot << endl;
    cout << "The total average of the numbers is: " << avg_tot << endl;
    
//**********************
// Problem 3
//**********************
    cout << "******** Problem 3 ********" << endl;
    cout << endl;

    char I = 'I';
    char V = 'V';
    char X = 'X';
    char L = 'L';
    char C = 'C';
    char D = 'D';
    char M = 'M';
    
    int ara_year; // Arabic (yyyy) year.
    
    string answer;
    
    cout << "Hello, this program will convert a four-digit Arabic (ordinary)"
            " year (yyyy) into a year written in Roman Numerals." << endl;
    cout << "Would you like to run this program? (Enter 'Yes' to start, or 'No'"
            " to cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter a four-digit Arabic (yyyy) year to convert: ";
        cin >> ara_year;
        cout << ara_year << " in Roman Numerals is: ";
        
        int M_year = ara_year / 1000;
        
        int D_year = (ara_year % 1000);
        ara_year %= 500;
        int D_num = ara_year / 500;

        int C_year = (ara_year % 100);
        ara_year %= 100;
        int C_num = ara_year / 100;
  
        int L_year = (ara_year % 50);
        int L_num = ara_year / 50;
        ara_year %= 50;
        
        int X_year = (ara_year % 10);
        int X_num = ara_year / 10;
        ara_year %= 10;
        
        int V_year = (ara_year % 5);
        int V_num = ara_year / 5;
        ara_year %= 5;
        
        int I_year = (ara_year % 1);

        if(M_year >= 0)
        {
            for(int M_count = 0; M_count < M_year; M_count++)
            {
                cout << M;
            }
        }
        if(D_year >= 400)
        {
            if(D_year <= 999 || D_year >= 900)
            {
                if(D_year % 900 >= 0 && D_year % 900 <= 99)
                {
                    cout << C << M;
                }
                else
                {
                    cout << D;
                }
            }
            else if(D_year <= 499 || D_year >= 400)
            {
                cout << C << D;
            }
            else
            {
                for(int D_count = 0; D_count < D_num; D_count++)
                {
                    cout << D;
                }
            }
        }
        if(C_year >= 0)
        {
            if(C_year == 90)
            {
                cout << X << C;
            }
            else
            {
                for(int C_count = 0; C_count < C_num; C_count++)
                {
                    cout << C;
                }
            }
        }
        if(L_year >= 0 || C_year == 50)
        {
            if(C_year == 99)
            {
                cout << X << C;
            }
            else if(C_year == 60)
            {
                cout << L << X;
            }
            else if(L_year == 40)
            {
                cout << X << L;
            }
            else
            {
                for(int L_count = 0; L_count < L_num; L_count++)
                {
                    cout << L;
                }
            }
        }
        if(X_year >= 0)
        {

                for(int X_count = 0; X_count < X_num; X_count++)
                {
                    cout << X;
                }
            
        }
        if(V_year >= 0)
        {
            
            if(X_year == 9)
            {
                cout << I << X;
            }
            else if(V_year == 4)
            {
                cout << I << V;
            }
            else if(V_year % 5 < 4)
            {
                for(int I_count = 0; I_count < V_year; I_count++)
                {
                    cout << I;
                }
            }
            else
            {
                for(int V_count = 0; V_count < 1; V_count++)
                {
                    cout << V;
                }
            }
        }
        cout << endl << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to run the program again? (Enter 'Yes' to"
                    " run again, or 'No' to terminate): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you for using the program." << endl;
        }
        
    }

//**********************
// Problem 4
//**********************
    cout << "******** Problem 4 ********" << endl;
    cout << endl;

    double weight;
    double height;
    double age;
    double bmr; // Basal Metabolic Rate (unit: calories).
    
    int choc_bar = 230; // 230 calories.
    
    char gender;
    string answer;
    
    // Prompts user to start program.
    cout << "Hello, this program will calculate your Basal Metabolic Rate (BMR)"
            " for your specified gender, age, weight, and height." << endl
            << "It will also output the number of chocolate bars to maintain"
            " your weight relative to your BMR." << endl;
    cout << "Would you like to run this program? (Enter 'Yes' to run, or 'No'"
            " to cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter your weight in pounds: ";
        cin >> weight;
        cout << endl;
        
        cout << "Please enter your height in inches: ";
        cin >> height;
        cout << endl;
        
        cout << "Please enter your age in years: ";
        cin >> age;
        cout << endl;
        
        cout << "Lastly, please enter your gender ('M' for male,"
                " 'F' for female): ";
        cin >> gender;
        cout << endl;
        
        if(gender == 'M')
        {
            bmr = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
            
            cout << "Your BMR relative to your inputs is: " << bmr << endl;
            
            double max_choc = bmr / choc_bar;
            
            cout << "You can consume a max of " << fixed << setprecision(2) <<
                    max_choc << " chocolate"
                    " bars to maintain your current weight." << endl;
            answer = "no";
        }
        else if(gender == 'F')
        {
            bmr = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
            
            cout << "Your BMR relative to your inputs is: " << bmr << endl;
            
            double max_choc = bmr / choc_bar;
            
            cout << "You can consume a max of " << fixed << setprecision(2) <<
                    max_choc << " chocolate"
                    " bars to maintain your current weight." << endl;
            answer = "no";
        }
        else
        {
            cout << "You did not enter a valid gender." << endl;
            answer = "Yes";
        }
        
        if(answer == "no")
        {
            cout << "Would you like to run the program again? ('Yes' to run"
                    " again, 'No' to terminate): ";
            cin >> answer;
            cout << endl;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you, the program will terminate." << endl;
        }
    }

//**********************
// Problem 5
//**********************
    cout << "******** Problem 5 ********" << endl;
    cout << endl;
    
    int assignments;
    int score;
    int tot_score;
    int sum_score = 0;
    int sum_total = 0;
    double tot_perc;
    
    string answer;
    
    // Prompts user to start program.
    cout << "Hello, this program will calculate your total grade (and "
            "percentage) for the amount of assignments and grades you input."
            << endl;
    cout << "Would you like to runt his program? (Enter 'Yes' to run, or 'No'"
            " to cancel): ";
    cin >> answer;
    
    while(answer == "Yes" || answer == "yes")
    {
        cout << "Please enter how many assignments there are: ";
        cin >> assignments;
        cout << endl;
        
        cout << "Enter the following:" << endl;
        cout << endl;
        
        for(int i = 0; i < assignments; i++)
        {
            cout << "Score for assignment " << i + 1 << ": ";
            cin >> score;
            
            sum_score += score;
            
            cout << "Total points possible for assignment " << i + 1 << ": ";
            cin >> tot_score;
            
            sum_total += tot_score;
        }
        
        tot_perc = (static_cast<double>(sum_score) 
                / static_cast<double>(sum_total)) * 100;
        
        cout << "Your total is " << sum_score << " out of " << sum_total
                << ", or a " << fixed << setprecision(2) << tot_perc
                << "%." << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to run the program again? ('Yes' to run,"
                    " 'No' to terminate): ";
            cin >> answer;
            cout << endl;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Thank you, the program will terminate." << endl;
        }
    }

//**********************
// Problem 6 / Problem 7
//**********************
    cout << "******** Problem 6 & 7 ********" << endl;
    cout << endl;
    
    string paper = "P";
    string scissors = "S";
    string rock = "R";
    
    int player1_wins = 0;
    int player2_wins = 0;
    
    string player1;
    string player2;
    
    string answer;
    
    // Prompts user to play game.
    cout << "Hello, this is a game of rock, paper, and scissors." << endl;
    cout << "This is a two-player game whom verses each other." << endl;
    cout << "Would you like to play? (Enter 'Yes' to start, or 'No' to cancel)"
            ": ";
    cin >> answer;
    
    cout << "Remember: paper covers rock, rock break scissors, "
                "scissors cut paper, or if the same choice, nobody wins."
                << endl;
    cout << endl;
        
    while(answer == "Yes" || answer == "yes")
    {

        cout << "Player 1, please select your choice: "
                << "Enter 'P' for paper, 'S' for scissors, or 'R' for rock."
                << endl;
        cout << "Player 1: ";
        cin >> player1;
        
        cout << "Player 2, please select your choice: "
                << "Enter 'P' for paper, 'S' for scissors, or 'R' for rock."
                << endl;
        cout << "Player 2: ";
        cin >> player2;

        if(player1 == "P" || player1 == "p")
        {
            if(player2 == "S" || player2 == "s")
            {
                cout << "Scissors beats paper! Player 2 wins." << endl;
                player2_wins++;
            }
            else if(player2 == "R" || player2 == "r")
            {
                cout << "Paper beats rock! Player 1 wins." << endl;
                player1_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(player1 == "S" || player1 == "s")
        {
            if(player2 == "P" || player2 == "p")
            {
                cout << "Scissors beats paper! Player 1 wins." << endl;
                player1_wins++;
            }
            else if(player2 == "R" || player2 == "r")
            {
                cout << "Rock beats scissors! Player 2 wins." << endl;
                player2_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(player1 == "R" || player1 == "r")
        {
            if(player2 == "S" || player2 == "s")
            {
                cout << "Rock beats scissors! Player 1 wins." << endl;
                player1_wins++;
            }
            else if(player2 == "P" || player2 == "p")
            {
                cout << "Paper beats rock! Player 2 wins." << endl;
                player2_wins++;
            }
            else
            {
                cout << "Tie! Nobody wins." << endl;
            }
        }
        cout << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to play again? ('Yes' to keep playing, or"
                    " 'No' to stop): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Player 1 won " << player1_wins << " match(es)." << endl;
            cout << "Player 2 won " << player2_wins << " match(es)." << endl;
            cout << "Thank you for playing!" << endl;
        }
    }

//**********************
// Problem 8
//**********************
    cout << "******** Problem 8 ********" << endl;
    cout << endl;
    
    srand(time(0));
    int paper = 1;
    int scissors = 2;
    int rock = 3;
    
    int comp1_wins = 0;
    int comp2_wins = 0;
    
    
    string answer;
    
    // Prompts user to play game.
    cout << "Hello, this is a game of rock, paper, and scissors." << endl;
    cout << "This is a two-player *computer* game whom verses each other." 
            << endl;
    cout << "Would you like to play? (Enter 'Yes' to start, or 'No' to cancel)"
            ": ";
    cin >> answer;
    
    cout << "Remember: paper covers rock, rock break scissors, "
                "scissors cut paper, or if the same choice, nobody wins."
                << endl;
    cout << endl;
        
    while(answer == "Yes" || answer == "yes")
    {

        cout << "Computer 1 is choosing..." << endl;
        int comp1 = rand() % 3 + 1;
        
        cout << "Computer 2 is choosing..." << endl;
        int comp2 = rand() % 3 + 1;
        
        if(comp1 == 1)
        {
            if(comp2 == 2)
            {
                cout << "Computer 1 chose paper!" << endl;
                cout << "Computer 2 chose scissors!" << endl;
                
                cout << "Scissors beats paper! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else if(comp2 == 3)
            {
                cout << "Computer 1 chose paper!" << endl;
                cout << "Computer 2 chose rock!" << endl;
                
                cout << "Paper beats rock! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose paper!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(comp1 == 2)
        {
            if(comp2 == 1)
            {
                cout << "Computer 1 chose scissors!" << endl;
                cout << "Computer 2 chose paper!" << endl;
                
                cout << "Scissors beats paper! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else if(comp2 == 3)
            {
                cout << "Computer 1 chose scissors!" << endl;
                cout << "Computer 2 chose rock!" << endl;
                
                cout << "Rock beats scissors! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose scissors!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        if(comp1 == 3)
        {
            if(comp2 == 2)
            {
                cout << "Computer 1 chose rock!" << endl;
                cout << "Computer 2 chose scissors!" << endl;
                
                cout << "Rock beats scissors! Computer 1 wins." << endl;
                comp1_wins++;
            }
            else if(comp2 == 1)
            {
                cout << "Computer 1 chose rock!" << endl;
                cout << "Computer 2 chose paper!" << endl;
                cout << "Paper beats rock! Computer 2 wins." << endl;
                comp2_wins++;
            }
            else
            {
                cout << "Computer 1 AND Computer 2 chose rock!" << endl;
                cout << "Tie! Nobody wins." << endl;
            }
        }
        cout << endl;
        answer = "no";
        
        if(answer == "no")
        {
            cout << "Would you like to play again? ('Yes' to keep playing, or"
                    " 'No' to stop): ";
            cin >> answer;
        }
        if(answer == "No" || answer == "no")
        {
            cout << "Computer 1 won " << comp1_wins << " match(es)." << endl;
            cout << "Computer 2 won " << comp2_wins << " match(es)." << endl;
            cout << "Thank you for playing!" << endl;
        }
    }
    return 0;
}