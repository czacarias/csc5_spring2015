/*
* Name: Charlie Zacarias
* Student ID: 2493977
* Date: March 16, 2015
* HW: 03
* Problem:
* I certify this is my own work and code.
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char** argv) 
{
//**********************
// Problem 1
//**********************
    string phone_number;        
 
    // Prompts user to enter number
    cout << "Please enter your telephone number: ";
    cin >> phone_number;
    
    int loc = phone_number.find("-");
    
    cout << "You have entered: " << phone_number.substr(0,loc)
                                 << phone_number.substr(loc + 1); 
    cout << endl;
    
//**********************
// Problem 2
//**********************
    int apples, oranges, pears;
    
    // Prompts user to enter the number of apples, oranges, and pears.
    cout << "Please enter the number of apples you have: ";
    cin >> apples;
    cout << "Please enter the number of oranges you have: ";
    cin >> oranges;
    cout << "Please enter the number of pears you have: ";
    cin >> pears;
    
    // To output how many apples, oranges, and pears the user should leave
    // so that the number of apples = oranges = pears.
    int sum_Fruit, diff_App, diff_Ora, diff_Pear;
    
    sum_Fruit = apples + oranges + pears;
    diff_App = sum_Fruit - apples;
    diff_Ora = sum_Fruit - oranges;
    diff_Pear = sum_Fruit - pears;
    
    if(diff_App > diff_Ora && diff_App > diff_Pear)
    {
        cout << "The number of apples you should leave is: " << apples - apples
                << "." << endl;
        cout << "The number of oranges you should leave is: " << oranges - apples
                << "." << endl;
        cout << "The number of pears you should leave is: " << pears - apples
                << "." << endl;
        cout << endl;
    }
    else if(diff_Ora > diff_App && diff_Ora > diff_Pear)
    {
        cout << "The number of apples you should leave is: " << apples - oranges
                << "." << endl;
        cout << "The number of oranges you should leave is: " << oranges - oranges
                << "." << endl;
        cout << "The number of pears you should leave is: " << pears - oranges
                << "." << endl;
        cout << endl;
    }
    else if(diff_Pear > diff_App && diff_Pear > diff_Pear)
    {
        cout << "The number of apples you should leave is: " << apples - pears
                << "." << endl;
        cout << "The number of oranges you should leave is: " << oranges - pears
                << "." << endl;
        cout << "The number of pears you should leave is: " << pears - pears
                << "." << endl;
        cout << endl;
    }
    else
    {
        cout << "You did not enter valid numbers." << endl;
    }
    
//**********************
// Problem 3
//**********************
    int max_cap;
    int num_people;
    
    // Prompts user to input the room's max capacity and number of people
    // attending.
    cout << "Please enter the meeting room's maximum capacity: ";
    cin >> max_cap;
    cout << "Please enter the number of people attending the meeting: ";
    cin >> num_people;
    
    if(max_cap > num_people || max_cap == num_people)
    {
        cout << "The meeting is within fire law regulations." << endl;
        cout << endl;
    }
    else if(max_cap < num_people)
    {
        int exclude;
        exclude = num_people - max_cap;
        cout << "The meeting cannot be held due to violation in fire\n"
                "law regulations." << endl;
        cout << "Please execute " << exclude << " people in order for the\n"
                "meeting room to be within fire law regulations." << endl;
        cout << endl;
    }
    else
    {
        cout << "You did not enter a valid input." << endl;
    }
    
//**********************
// Problem 4
//**********************
    const int size = 10; // Const int. "size = 10" is always 10.
    int num[size]; // "[size]" values numbers to a max of "size" (which is 10).
    int sum_pos = 0; // Sum > 0
    int sum_neg = 0; // Sum < 0
    int sum_tot; // Sum of all #s
 
    
    // Prompts user to enter 10 whole numbers.
    cout << "Please enter ten whole numbers:" << endl;
    
    // User inputs 10 whole numbers; negative or positive.
    for(int i = 0; i < size; i++)
    {
        cout << i + 1 << ") ";
        cin >> num[i];
        
        if(num[i] >= 0)
        {
            sum_pos += num[i];
        }
        else //if(num < 0 && i == 10)
        {
            sum_neg += num[i];
        }
    }
    
    // Outputs numbers entered. "[i]" outputs the i'th number entered above.
    cout << "The numbers you entered were: ";
    for(int i = 0; i < size; i++ )
    {
         cout << num[i] << ", ";
    }
    sum_tot = sum_pos + sum_neg;
    cout << endl;
    cout << "The sum of the positive numbers is: " << sum_pos << endl;
    cout << "The sum of the negative numbers is: " << sum_neg << endl;
    cout << "The total sum of the numbers is: " << sum_tot << endl;
    
    return 0;
}

