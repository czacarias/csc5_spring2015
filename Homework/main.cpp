/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2015, 11:17 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Creation of variable.
    string helloText;
    
    // Definition of variable.
    helloText = "Hello, my name is Hal!";
    
    // Output.
    cout << helloText << endl;
    
    // Input.
    // Hal asks for user's name.
    string userInput;
    cout << "What is your name?" << endl;
    cin >> userInput;
    
    // Response.
    cout << "Hello, " << userInput << ". I am glad to meet you.";
         
    return 0;
}

