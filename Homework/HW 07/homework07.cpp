/*
* Name: Charlie Zacarias
* Student ID: 2493977
* Date: May 3, 2015
* HW: 07
* Problem: 1 - 9
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

int maxVal(const vector<int>&);

int minVal(const vector<int>&);

int evenVal(const vector<int>&);

double medVal(const vector<int>&);

void mergeFile(ifstream&, ifstream&, ofstream&);

void sortFile(ofstream&);

int maxVal9(const vector<int>&);

void outputVect(const vector<int>&);

int main(int argc, char** argv) 
{
//**********************
// Problem 1 / 2 / ~3
//**********************
    cout << "***** Problem 1 / 2 / ~3 *****" << endl;
    cout << endl;
    
    cout << "Please enter a file name: ";
    
    string fileName;
    
    cin >> fileName; // "data.dat" in this problem.
    
    ifstream infile;
    infile.open(fileName.c_str());
    
    while(!infile) // File checking - Checks if file exists.
    {
        cout << "ERROR! File does not exist." << endl;
        cout << "Please enter a valid file name: ";
        cin >> fileName;
        infile.open(fileName.c_str());
    }
    
    vector<int> data;
    
    if(infile)
    {
        int value;
        
        while(infile >> value) // Reads elements from file to the vector.
        {
            data.push_back(value);
        }
    }
    infile.close();
    
    cout << "The max value of the file is: " << maxVal(data) << endl;
    cout << "The min value of the file is: " << minVal(data) << endl;
    cout << "The file contains " << evenVal(data) << " evens." << endl;
  
//**********************
// Problem 3
//**********************
    cout << "***** Problem 3 *****" << endl;
    cout << endl;
  
    cout << "Please enter a file name: ";
    
    string fileName1;
    
    cin >> fileName1; // "data1.dat," "data2.dat," or "data3.dat".
    
    ifstream infile1;
    infile1.open(fileName1.c_str());
    
    while(!infile1) // File checking - Checks if file exists.
    {
        cout << "ERROR! File does not exist." << endl;
        cout << "Please enter a valid file name: ";
        cin >> fileName1;
        infile1.open(fileName1.c_str());
    }
    
    vector<int> data1;
    
    if(infile1)
    {
        int value;
        
        while(infile1 >> value) // Reads elements from file to the vector.
        {
            data1.push_back(value);
        }
    }
    infile1.close();
    
    cout << "The file contains " << evenVal(data1) << " evens." << endl;
    
//**********************
// Problem 4
//**********************
    cout << "***** Problem 4 *****" << endl;
    cout << endl;
    
    cout << "Please enter a file name: ";
    
    string fileName2; // Either "datamedeven.dat" or "datamedodd.dat".
    
    cin >> fileName2;
    
    ifstream infile2;
    infile2.open(fileName2.c_str());
    
    while(!infile2)
    {
        cout << "ERROR! File does not exist." << endl;
        cout << "Please enter a valid file name: ";
        cin >> fileName2;
        infile2.open(fileName2.c_str());
    }
    
    vector<int> dataMed;
    
    if(infile2)
    {
        int val;
        
        while(infile2 >> val) // Reads elements from file to the vector.
        {
            dataMed.push_back(val);
        }
    }
    infile2.close();
    
    cout << "The median of the file is: " << medVal(dataMed) << endl;

//**********************
// Problem 5
//**********************
    cout << "***** Problem 5 *****" << endl;
    cout << endl;
    
    cout << "Please enter a file name: ";
    
    string fileName3;
    
    cin >> fileName3;
    
    ifstream infile3;
    infile3.open(fileName3.c_str());
    
    while(!infile3)
    {
        cout << "ERROR! File not found." << endl;
        cout << "Please enter a valid file name: ";
        cin >> fileName3;
        infile3.open(fileName3.c_str());
    }
    
    vector<string> sent;
    
    if(infile3)
    {
        string word;
        
        while(infile3 >> word)
        {
            sent.push_back(word);
        }
    }
    
    infile3.close();
    
    ofstream outfile3;
    outfile3.open("prob5edit.txt");
    
    for(int i = 0; i < sent.size(); i++)
    {
        if(sent[i] != " ")
        {
            outfile3 << sent[i];
        }
        outfile3 << " ";
    }

    outfile3.close();
    
//**********************
// Problem 6 - Having Compiling issues.
//**********************
    cout << "***** Problem 6 *****" << endl;
    cout << endl;
    
    
    ofstream outfile;
    cout << "Please create a main file to be the product of two files: ";
    string mergerFile;
    cin >> mergerFile; // Should be "out.dat".
    outfile.open(mergerFile.c_str());
    outfile.close();
    
    cout << "Please enter the first file name: ";
    
    string firstFile;
    
    cin >> firstFile;
    
    cout << "Please enter the second file name: ";
    
    string secondFile;
    
    cin >> secondFile;
    
    mergeFile(firstFile, secondFile, mergerFile);
    
    sortFile(mergerFile);

//**********************
// Problem 7 / 8
//**********************
    cout << "***** Problem 7 / 8 *****" << endl;
    cout << endl;
    
    vector<int> v_7;
    int odds = 0;
    
    for(int i = 0; i < 10; i++)
    {
        int randVal = rand() % 50;
        
        if(randVal % 2 !=0)
            odds++;
        
        v_7.push_back(randVal);
    }
    
    cout << "There are " << odds << " odds in the vector." << endl;
    
    for(int i = 0; i < 10; i++)
    {
        cout << v_7[i] << " ";
    }

//**********************
// Problem 9
//**********************
    cout << "***** Problem 9 *****" << endl;
    cout << endl;
    
    vector<int> v_9;
    
    for(int i = 0; i < 20; i++)
    {
        int randVal = rand() % 50;
        v_9.push_back(randVal);
    }
    
    cout << "The max value of the vector is: " << maxVal9(v_9) << endl;
    
    outputVect(v_9);
    
    return 0;
}

// Problem 1 / 2 / 3 Functions.
int maxVal(const vector<int>& v)
{
    int max = 0;
    
    for(int i = 0; i < v.size(); i++)
        if(max < v[i])
            max = v[i]; // Appends max to be value v[i] for next iteration.
        
    return max;
}

int minVal(const vector<int>& v)
{
    int min = 1; // Initializes min at 1 since at 0 the if-statement is
                 // always false and never appends the min.
    
    for(int i = 0; i < v.size(); i++)
        if(min > v[i])
            min = v[i];
        
    return min;
}

int evenVal(const vector<int>& v)
{
    int evens = 0;
    
    for(int i = 0; i < v.size(); i++)
        if(v[i] % 2 == 0)
            evens++;
    
    return evens;
}

// Problem 4 Function.
double medVal(const vector<int>& v)
{
    if(v.size() % 2 == 0)
    {
        for(int i = 0; i < v.size(); i++)
        {
            for(int j = v.size(); j > 0; j--)
            {
                if(i == j-1)
                {
                    return (static_cast<double>(v[i]) 
                            + static_cast<double>(v[j])) / 2;
                }
            }
        }
    }
    else
    {
        return v[v.size() / 2];
    }
}

// Problem 6 Function.
//void mergeFile(ifstream& file1, ifstream& file2, ofstream& out)
//{
//    // File 1.
//    ifstream infile1;
//    infile1.open(file1.c_str());
//    
//    vector<int> v_1;
//    
//    if(infile1)
//    {
//        int value;
//        
//        while(infile1 >> value)
//        {
//            v_1.push_back(value);
//        }
//    }
//    infile1.close();
//    
//    // File 2.
//    ifstream infile2;
//    infile2.open(file2.c_str());
//    
//    vector<int> v_2;
//    
//    if(infile2)
//    {
//        int value;
//        
//        while(infile2 >> value)
//        {
//            v_2.push_back(value);
//        }
//    }
//    infile2.close();
//    
//    // Outfile.
//    ofstream outfile;
//    outfile.open(out.c_str());
//    
//    for(int i = 0; i < v_1.size() + v_2.size(); i++)
//    {
//        outfile << v_1[i] << " " << v_2[i];
//    }
//    outfile.close();
//}

//void sortFile(ofstream& out)
//{
//    ifstream infile;
//    infile.open(out.c_str());
//    
//    vector<int> v_3;
//    
//    if(infile)
//    {
//        int value;
//        
//        while(infile >> value)
//        {
//            v_3.push_back(value);
//        }
//    }
//    infile1.close();
//    
//    int length = v_3.size();
//    
//    for (int i = 0; i < length; i++)
//    {
//        for (int j = 0; j < length - (i+1); j++)
//        {
//            if (v_3[j] > v_3[j+1])
//            {
//                int temp = v_3[i];
//                v_3[i] = v_3[j];
//                v_3[j] = temp;
//            }
//        }
//    }
//}

/* Problem 9 Function. */
int maxVal9(const vector<int>& v)
{
    int max = 0;
    
    for(int i = 0; i < 20; i++)
        if(max < v[i])
            max = v[i]; // Appends max to be value v[i] for next iteration.
        
    return max;
}

void outputVect(const vector<int>& v)
{
    for(int i = 0; i < 20; i++)
    {
        cout << v[i] << " ";
    }
}